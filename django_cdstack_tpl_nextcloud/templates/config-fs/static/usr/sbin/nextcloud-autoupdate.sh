#!/bin/bash

sudo -u www-data php --define apc.enable_cli=1 /var/www/nextcloud/updater/updater.phar -n

sudo -u www-data php --define apc.enable_cli=1 /var/www/nextcloud/occ upgrade -n

sudo -u www-data php --define apc.enable_cli=1 /var/www/nextcloud/occ app:update --all -n

sudo -u www-data php --define apc.enable_cli=1 /var/www/nextcloud/occ db:add-missing-columns -n
sudo -u www-data php --define apc.enable_cli=1 /var/www/nextcloud/occ db:add-missing-indices -n
sudo -u www-data php --define apc.enable_cli=1 /var/www/nextcloud/occ db:add-missing-primary-keys -n
sudo -u www-data php --define apc.enable_cli=1 /var/www/nextcloud/occ db:convert-filecache-bigint -n
sudo -u www-data php --define apc.enable_cli=1 /var/www/nextcloud/occ db:convert-mysql-charset -n

sudo -u www-data php --define apc.enable_cli=1 /var/www/nextcloud/occ maintenance:mode --off -n
